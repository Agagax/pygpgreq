#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

"""
Created on 22 mars 2015

@author: L

Script which mimics the output found on "keyserver.ubuntu.net" when
searching for keys information. One can add any key (8-bytes hexadecimal
coded) or "openpkg" for a list of keys (for the 'openpkg' project).

Although one can add any project name ('hexchat', 'thunderbird', etc.),
for Python studying purpose only 'openpkg' has been retained as valid search.

for information, we have to send something like:
http://keyserver.ubuntu.net/pks/lookup?op=vindex&search=0xAAAAAAAA+0x12345678&fingerprint=on
    where AAAAAAAA and 12345678 are the key we want to check info from.

The script will catch on command line all arguments.
 * invalid "keys", i.e. not hexadecimal and not 8-bytes long will be discarded.
 * "0x" will be added to every valid keys found on command line parameter.
 * "openpkg" is a valid "key" to search for (no '0x' added for that one).
"""

from bs4 import BeautifulSoup
import re
from sys import argv
import requests

# ============================================================================
# Regex used to filter output.
#
# BeautifulSoup output the following annoying text:
#     /**/
#      .uid { color: green; text-decoration: underline; }
#      .warn { color: red; font-weight: bold; }
#     /**/
# The below regex remove:
#    - "/**/" lines,
#    - ' .xxxxxxxx' lines,
#    - part of line showing "Search results for '0x12345678'"
#    - full lines if "No results found." inside.
#
# As many "empty" lines will remains, the last regex is used to suppress them
# all. Empty lines can be '\n' or '\r'. Note: no empty line with only spaces
# or tab here, so no regex to remove such lines.
# ============================================================================
# regex to match ' .xxxx' or '/**/'
my_re1 = re.compile(r'(\s\..*)|(/\*\*/)')
# regex to match "Search results for '<whatever>'" or "No results
# found<whatever>"
my_re2 = re.compile(r'(Search results for \'.*\')*|(No results found.*)')
# regexp to match 2 or more '\n\n+' or '\n\r+' in a row
my_re3 = re.compile('\n[\n|\r]+')

# managing http error printout: \n after '):' or '>:'
re_erno = re.compile('[)>]:')

# proxies : brutal proxy encoding
proxy_dict = {}
use_proxy = False
if use_proxy:
    proxy_dict = {
        "http": "http://205.167.7.126:80",
        "https": "http://205.167.7.126:80",
        "ftp": "http://205.167.7.126:80"
    }

# Keyserver link: I chose the Ubuntu one
my_key_server = "http://keyserver.ubuntu.net/pks/lookup?op=vindex&search="


#####################
# FUNCTIONS
#####################


def deb(*args):
    """Debug printing: print and exit"""
    print(args)
    exit()


def usage():
    """How to use"""
    print("\tUsage: pygpgreq.py <key_1> ... <key_n>")
    print(
        "\t\twith <key_n> being the key number you want information \
for (coded on 8 hexadecimal digits)")
    print("\t\tor 'openpkg' to get the first page of `openpackage' keys.")


def read_keys():
    """ Read keys passed as arguments."""
    if len(argv) < 2:  # no arguments
        usage()
        exit()

    returned_args = []  # init returned list

    # filling in my_tmp_argv + all in lower case
    my_tmp_args = [arg.lower() for arg in argv]

    # Removing "0x" from any key (added later) just to ease the coding
    re_arg = re.compile("0x")
    my_tmp_args = [re_arg.sub('', arg) for arg in my_tmp_args]

    # Check for arg = "openpkg" in which case it will be added at later time
    # as "hexadecimal" checking will remove it even if it exists.
    if "openpkg" in my_tmp_args:
        is_openpkg = True
    else:
        is_openpkg = False

    # Checking keys are hexadecimal. "openpkg" is removed in the process.
    # keys are coded on a 8 bytes hexa number
    re_hexa = re.compile("([a-f]|[0-9]){8}$")  # $ to not have more than 8
    for arg in my_tmp_args:
        # if key is hexadecimal 8 bytes -> valid number -> we add it
        if re_hexa.match(arg):
            returned_args.append(arg)

    # Removing duplicates - Ordering will NOT be kept
    returned_args = list(set(returned_args))

    # Adding "0x" to each key
    returned_args = ["0x" + key for key in returned_args]

    # Adding "openpkg" if it was previously detected
    if is_openpkg:
        returned_args.append("openpkg")

    # Final re-test for empty valid list
    if not returned_args:
        print("    *** No valid key found. Exiting. ***")
        usage()
        exit()

    # All in order, returning information
    return returned_args


#####################
# MAIN
#####################


def print_erno(erno):
    """ requests exception information are rather long.
    We will insert a '\n' after each '>:' or '):'. """
    str_to_print = re_erno.sub(':\n ', erno)
    print(str_to_print)


def main():
    my_keys_to_check = read_keys()
    for ssh_key in my_keys_to_check:

        # Building internet link
        my_http_link = my_key_server + ssh_key + "&fingerprint=on"
        print("===[Results for {}]========================".format(ssh_key))

        # Sending request and grabbing result.
        try:
            f = requests.get(my_http_link, proxies=proxy_dict)
        except (requests.ConnectionError,
                requests.HTTPError,
                requests.Timeout,
                requests.TooManyRedirects) as erno:
            print("   *** HTTP problem: unable to get answer for {} ***\n"
                  "Cause:"
                  .format(ssh_key))
            # erno is a class. Extracting the string:
            print_erno(erno.__str__())
            continue
        # wild 'except': I don't know what to put here...
        except:
            print("  *** Unknown error.\n")
            continue

        my_content = f.text

        match = re.search("No results found", my_content)
        if match:
            print("   *** Key {} not found in database.\n".format(ssh_key))
            continue

        # Content of answer --> BeautifulSoup
        # "xml" remove a bit of CDATA, not all
        soup = BeautifulSoup(my_content, "xml")
        out_txt = soup.text  # keeping result

        # removing lines I don't want to print (see regex near top of script)
        to_print = my_re1.sub('', out_txt)
        to_print = my_re2.sub('', to_print)  # removing 'Search results ...'
        to_print = my_re3.sub('', to_print)  # removing blank lines

        print(to_print)  # printing result

    print("=== Completed ===")


if __name__ == "__main__":
    main()
